"""
This module is designed to aide in processing the mutations found for specific genes and applying them to the amino acid sequence. 
Ultimately, this will allow toquery PSOPIA or PSIVER webservers for interaction likelihoods between the mutants of a given gene and a target protein. 
"""

import pandas as pd
import re 

# ================================
# Functions to identify which kind
# of mutation is present in a 
# dataframe entry of phen
# ================================

def _multi_insertion(entry):
    pattern = r"([A-Z]{1}[a-z]{2})(\d+)_([A-Z]{1}[a-z]{2})(\d+)ins(([A-Z]{1}[a-z]{2})+)"
    r = re.search(pattern, entry)
    pre = r.group(1)
    p_pre = r.group(2)
    post = r.group(3)
    p_post = r.group(4)
    mut = r.group(5)
    mut = to_one_letter(mut, multiple = True)
    pre, post = to_one_letter(pre), to_one_letter(post)
    return pre, p_pre, mut

def _multi_duplication(entry):
    pattern = r"([A-Z]{1}[a-z]{2})(\d+)_([A-Z]{1}[a-z]{2})(\d+)dup"
    r = re.search(pattern, entry)
    pre = r.group(1)
    p_pre = r.group(2)
    post = r.group(3)
    p_post = r.group(4)
    pre, post = to_one_letter(pre), to_one_letter(post)
    mut = "mdup"
    return (pre, post), (p_pre, p_post), mut

def _single_duplication(entry):
    pattern = r"([A-Z]{1}[a-z]{2})(\d+)dup"
    r = re.search(pattern, entry)
    pre = r.group(1)
    p_pre = r.group(2)
    pre = to_one_letter(pre)
    mut = "dup"
    return pre, p_pre, mut

def _multi_deletion(entry):
    pattern = r"([A-Z]{1}[a-z]{2})(\d+)_([A-Z]{1}[a-z]{2})(\d+)del"
    r = re.search(pattern, entry)
    pre = r.group(1)
    p_pre = r.group(2)
    post = r.group(3)
    p_post = r.group(4)
    pre, post = to_one_letter(pre), to_one_letter(post)
    mut = "mdel"
    return (pre, post), (p_pre, p_post), mut

def _single_deletion(entry):
    pattern = r"([A-Z]{1}[a-z]{2})(\d+)del"
    r = re.search(pattern, entry)
    pre = r.group(1)
    p_pre = r.group(2)
    pre = to_one_letter(pre)
    mut = "del"
    return pre, p_pre, mut


def _multi_inversion(entry):
    pattern = r"([A-Z]{1}[a-z]{2})(\d+)_([A-Z]{1}[a-z]{2})(\d+)inv"
    r = re.search(pattern, entry)
    pre = r.group(1)
    p_pre = r.group(2)
    post = r.group(3)
    p_post = r.group(4)
    pre, post = to_one_letter(pre), to_one_letter(post)
    mut = "inv"
    return (pre, post), (p_pre, p_post), mut

def _get_muts(pattern, entry):
    r = re.search(pattern, entry)
    pre = r.group(1)
    pos = int(r.group(2))
    mut = r.group(3)
    pre, mut = to_one_letter(pre), to_one_letter(mut)
    return pre, pos, mut 

def _stop_gain(entry):
    pattern = r"([A-Z]{1}[a-z]{2})(\d+)(\*)"
    pre, pos, mut = _get_muts(pattern, entry)
    return pre, pos, mut

def _single_substitute(entry):
    pattern = r"([A-Z]{1}[a-z]{2})(\d+)([A-Z]{1}[a-z]{2})"
    pre, pos, mut = _get_muts(pattern, entry)
    return pre, pos, mut

def _stop_loss(entry): 
    pattern = r"(\*)(\d+)([A-Z]{1}[a-z]{2})"
    r = re.search(pattern, entry)
    pre, pos, mut = _get_muts(pattern, entry)

    return pre, pos, mut

# this dictionary will allow assignment of the appropriate decoders...
decoders = {
    r"([A-Z]{1}[a-z]{2})(\d+)([A-Z]{1}[a-z]{2})" : _single_substitute,
    r"(\*)(\d+)([A-Z]{1}[a-z]{2})" : _stop_loss, 
    r"([A-Z]{1}[a-z]{2})(\d+)(\*)" : _stop_gain, 
    r"([A-Z]{1}[a-z]{2})(\d+)_([A-Z]{1}[a-z]{2})(\d+)ins([A-Z]{1}[a-z]{2})+" : _multi_insertion, 
    r"([A-Z]{1}[a-z]{2})(\d+)_([A-Z]{1}[a-z]{2})(\d+)dup" : _multi_duplication,
    r"([A-Z]{1}[a-z]{2})(\d+)dup" : _single_duplication,
    r"([A-Z]{1}[a-z]{2})(\d+)_([A-Z]{1}[a-z]{2})(\d+)del" : _multi_deletion, 
    r"([A-Z]{1}[a-z]{2})(\d+)del" : _single_deletion,
    r"([A-Z]{1}[a-z]{2})(\d+)_([A-Z]{1}[a-z]{2})(\d+)inv" : _multi_inversion,

}

AMINO_ACID_CODES = {
    'Cys': 'C',
    'Asp': 'D',
    'Ser': 'S',
    'Gln': 'Q',
    'Lys': 'K',
    'Ile': 'I',
    'Pro': 'P',
    'Thr': 'T',
    'Phe': 'F',
    'Asn': 'N',
    'Gly': 'G',
    'His': 'H',
    'Leu': 'L',
    'Arg': 'R',
    'Trp': 'W',
    'Ala': 'A',
    'Val':'V',
    'Glu': 'E',
    'Tyr': 'Y',
    'Met': 'M',
    "*" : "*"
    }

def to_one_letter(code, multiple = False):
    if multiple == False: 
        return AMINO_ACID_CODES[code]
    else: 
        code = [code[i:i+3] for i in range(0, len(code), 3)]
        code = [AMINO_ACID_CODES[i] for i in code]
        code = "".join(code)
        return code

def _get_decoder(entry):
    # this function will get the right decoder for a given mutation
    decoder = [decoders[i] for i in decoders if re.search(i , entry) is not None]
    if len(decoder) > 1: 
        print(Warning(f"Multiple decoders were found for this mutant: {entry} ! Returning first one: {decoder[0]}"))
    elif len(decoder) == 0:
        print(Warning(f"No decoders were found for this type of mutant: {entry}"))
        return None
    return decoder[0]


def get_p_mutation(entry):
    """
    This function gets the mutation in the protein sequence from a given entry in main_df... 
    """
    try: 
        entry = entry.replace("p.", "")
        decoder = _get_decoder(entry)
        pre, pos, mut = decoder(entry)
        return pre, pos, mut
    except: pass


# =================================================
# Functions to apply mutations to the sequence
# =================================================

def _invert_seq(ref_seq, start, stop):
    start, stop = int(start), int(stop)
    segment = ref_seq[start-1:stop]
    new_segment = segment[::-1]
    new_seq = list(ref_seq)
    j = 0
    for i in range(start-1, stop):
        new_seq[i] = new_segment[j]
        j += 1
    new_seq = "".join(new_seq)
    return new_seq

def _delete_seq(ref_seq, start, stop):
    start, stop = int(start), int(stop)
    new_seq = list(ref_seq)
    for i in range(start-1, stop):
        del new_seq[start-1]
        print("".join(new_seq))
    new_seq = "".join(new_seq)
    return new_seq

def _insert_seq(ref_seq, start, segment):
    start = int(start)
    new_seq = list(ref_seq)
    new_seq.insert(start-1, segment)
    new_seq = "".join(new_seq)
    return new_seq

def _insert_duplicate_seq(ref_seq, start, stop):
    start, stop = int(start), int(stop)
    new_seq = list(ref_seq)
    segment = ref_seq[start-1 : stop]
    segment = "".join(segment)
    new_seq.insert(start-1, segment)
    new_seq = "".join(new_seq)
    return new_seq

def _substitute_seq(ref_seq, start, mut):
    start = int(start)
    new_seq = list(ref_seq)
    new_seq[start-1] = mut
    new_seq = "".join(new_seq)
    return new_seq

def _trim_stops(seq):
    new_seq = seq.split("*")
    new_seq = new_seq[0]
    new_seq = "".join(new_seq) + "*"
    return new_seq

# this dictionary stores the methods for introducing 
# mutations into the sequence alongside with the required 
# parameters which will be applied using eval()
_multi_cases = {
    "inv" : (_invert_seq, "{'ref_seq' : ref_seq, 'start' : pos[0], 'stop' : pos[1]}"), 
    "mdel" : (_delete_seq, "{'ref_seq' : ref_seq, 'start' : pos[0], 'stop' : pos[1]}"), 
    "del" : (_delete_seq, "{'ref_seq' : ref_seq, 'start' : pos, 'stop' : pos}"), 
    "mdup" : (_insert_duplicate_seq, "{'ref_seq' : ref_seq, 'start' : pos[0], 'stop' : pos[1]}"), 
    "dup" : (_insert_duplicate_seq, "{'ref_seq' : ref_seq, 'start' : pos, 'stop' : pos}"), 
}


# ================================
# A class to wrap all up tidily
# ================================

class SNPApplier:
    def __init__(self, gene, data, ref_seq_file):
        self._gene = gene
        self._DATA = data
        self._phencol = self._check_phencol()
        
        self._sample_col = "name"
        self._get_entries()
        self._get_mutations()
        
        self._refseq = self._open_refseq(ref_seq_file)
        self._target_seq = None
        
        self.apply_mutations()

    def add_target(self, name, target_seq_file):
        """
        Allows to add an interaction partner (target) protein 
        fasta file to compile a fasta query text file for PSOPIA
        """
        target_seq = self._open_refseq(target_seq_file)
        self._target_seq = target_seq
        self._target_name = name

    def get_mutations(self):
        return self._mutations_df
    
    def get_entries(self):
        return self._ENTRIES
    
    def assemble_fasta(self, include_target = False):
        """
        This will assemble a fasta file of all mutant sequences
        including the ref_sequence and optionally the target
        """
        ref_string = f">ref\n{self._refseq}\n"
        compilation = self.compile(pair_sep="")
        compilation = ref_string + compilation
        compilation = compilation.replace(f">{self._target_name}\n{self._target_seq}\n", "")
        if include_target:
            target_string = f">{self._target_name}\n{self._target_seq}\n"
            compilation += target_string
        return compilation 

    def get_combined(self, compile = False, pair_sep="#"):
        try: 
            return self._combined_df
        except: 
            self.combine_per_sample()
            if compile: 
                return self._compilation_string(source = self._combined_df, name_col = "sample")
            return self._combined_df
    
    def compile(self, pair_sep="#"):
        """
        Generates a fasta pair string with all applied mutations and the target interactor for PSOPIA webserver
        """
        if self._target_seq is None: 
            print("You must first specify an interactor using add_taget()")
        else: 
            return self._compilation_string(source = self._mutations_df, pair_sep = pair_sep)

    def _compilation_string(self, source, name_col = "mutation", src_col = "seq", pair_sep = "#"):
        """
        Generates the string for pair-wise fasta in compile()
        """
        compilation = ""
        for mut, seq in zip(source[name_col], source[src_col]):
            tmp = f">{mut}\n{seq}\n>{self._target_name}\n{self._target_seq}\n{pair_sep}\n"
            compilation += tmp
        return compilation
    
    def combine_per_sample(self):
        samples = list(set(self._ENTRIES[self._sample_col]))
        combined = {}
        for sample in samples: 
            new_seq = list(self._refseq)
            last_applied = None
            for mutation in self._mutations:
                if f"@{sample}" in mutation: # check that only appropriate mutations are applied for each sample...
                    try: 
                        new_seq = self._apply_mutation(new_seq, mutation)
                        last_applied = mutation
                    except: 
                        print(f"Mutually exclusive mutation {last_applied} already applied, skipping: {mutation}")
            combined.update({sample : new_seq})
        
        self._combined = combined
        self._combined_df = pd.DataFrame({"sample" : combined.keys(), "seq" : combined.values()})
        

    def _open_refseq(self, filename):
        # reads in a single entry fasta file...
        with open(filename, "r") as openfile: 
            content = openfile.read().split("\n")[1:]
            content = "".join(content)
            return content
    
    def _check_phencol(self):
        # as there are currently two versions of the df around 
        # (one with p_phen one with phen as colname) we get the right one here...
        for i  in ["p_phen", "phen"]:
           if i in self._DATA.columns:
               return i 
       
    def _get_entries(self):
        """
        Queries the dataframe for all entries related to the gene
        """
        r = self._DATA.query(f"gene == '{self._gene}'")
        self._ENTRIES = r 

    def _get_mutations(self):
        """
        Get's all mutations from phen column and stores them in a dictionary
        """
        mutations = {}
        for mutant, name in zip(self._ENTRIES[self._phencol], self._ENTRIES[self._sample_col]):
            mutations.update({
                f"{mutant}@{name}" : get_p_mutation(mutant)
            })
        self._mutations = mutations

    def apply_mutations(self):
        seq_mutations = {}
        for mutation in self._mutations:
            new_seq = list(self._refseq)
            new_seq = self._apply_mutation(new_seq, mutation)
            seq_mutations.update({ mutation : new_seq })

        self._mutations_df = pd.DataFrame({"mutation" : seq_mutations.keys(), "seq" : seq_mutations.values()})

    def _apply_mutation(self, ref_seq, mutation):
        """
        This function applies one mutation to the protein sequence
        """
        pre, pos, mut = self._mutations[mutation]
        # if mut entry contains multiple amino acids it may be a 
        # special case like inversion or deletion or insertion
        if len(mut) > 1: 
            if mut in _multi_cases:
                method, params = _multi_cases[mut]
                new_seq = method(**eval(params))
            else: # insertion
                new_seq = _insert_seq(ref_seq, pos, mut)
        else: # a simple substitution
            new_seq = _substitute_seq(ref_seq, pos, mut)
            
        new_seq = _trim_stops(new_seq)

        return new_seq
    
    
