# How this file architecture works (pretty simple)

`source.tex` is the "main document" which simply imports the actual content and generates the document (ultimately). The only specifications here are title, authors and abstract...

`mainmatter` contains the actual sections of the report as separate files. 
Here the actual content is being written. 

`Ressources` is for any other documents/files that should be included such as images of plots etc. 

`Bibliography.bib`, well just the good-old bibliography...
