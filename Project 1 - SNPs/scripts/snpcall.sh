#!/bin/bash
#SBATCH --mail-type=fail
#SBATCH --job-name="snp call"
#SBATCH --nodes=1
#SBATCH --cpus-per-task=5
#SBATCH --time=3:00:00
#SBATCH --mem-per-cpu=6G
#SBATCH --partition=pcoursea

source /data/users/lfalquet/SBC07107_21/scripts/module.sh

USR=student02
USR_DIR=/data/users/$USR
READS_DIR=$USR_DIR/reads
cd $USR_DIR

# if snp folder is not yet present, make it
if [[ ! -d $USR_DIR/snp ]]; then
    mkdir $USR_DIR/snp
    cd $USR_DIR/snp
    ln -s /data/users/lfalquet/SBC07107_21/reference/R64-1-1.104.fa R64-1-1.104.fa
    ln -s /data/users/lfalquet/SBC07107_21/reference/R64-1-1.104.gtf R64-1-1.104.gtf
fi

cd $USR_DIR/snp

# link all *.bam files into folder as well... 
ln $READS_DIR/bwa*/*.bam $USR_DIR/snp

# now perform bcftools for each *bam file 
for XX in $(ls); do 
    
    # this should call a separate job to perform the code commented-out below...
    sbatch $USR_DIR/scripts/single_annotate.slurm $XX

    # # call SNPs for each strain (each bam file) and index the vcf.gz
    # bcftools mpileup -Ou --threads 8 -f R64-1-1.104.fa ${XX} | bcftools call -vc -Oz --threads 8 -o ${XX}.vcf.gz
    # tabix ${XX}.vcf.gz

    # # unzip the file for later use in annotation (will be removed after annotation)
    # gunzip -k ${XX}.vcf.gz

done
