# BioInf_Practical_HS21

### The Practical Repo for the Bioinformatics Practical 2021 
> Course: SBC.07107 Bioinformatics (practicals + in silico) [SA 21]

---

This repository documents the the workflow, data, and results of the first project of the Bioinformatics Practical: **Identification of Single-Nucleotide-Polymorphisms in Yeast (Saccharomyces cerevisiae)**.)

## Data Availability 
---
> #### Disclaimer: 
> Due to the filesize it is currently not possible to directly attach the source data folder to this repository
> However, the analysis is performed on a `csv` copy of the entire dataset's `vcf` results which can be found in the Analysis subfolder


## Methods in Brief
---
A brief overview of the workflow will be given here. Scripts used to perform the described steps are available in the `scripts` folder in this repository. 

### Initial Setup
Raw data was taken from the server directory and linked into a the local `reads` folder. In the following code fragments `$XX` denotes the mutant identifier $D_n$. 

 Processing steps with `fastqc` for quality control and `fastp` for cleanup were done using

```bash
#verify that your files are in your directory (ls -l)
fastqc -t 2 ${XX}_1.fastq.gz ${XX}_2.fastq.gz
```

Cleanup with `fastp` was done as

```bash
fastp -i ${XX}_1.fastq.gz -I ${XX}_2.fastq.gz -o ${XX}_1trim.fastq.gz -O ${XX}_2trim.fastq.gz -j ${XX}_fastp.json -h ${XX}_fastp.html --thread 4 --trim_poly_g -l 150;
```

This was followd by genome mapping of the sequencing reads to the reference genome was done with `samtools` using 

```bash 
samtools view -b -@ 4 -t R64-1-1.104.fa.fai ${XX}.sam > ${XX}_unsorted.bam
samtools sort -@ 4 -o ${XX}.bam ${XX}_unsorted.bam
samtools index ${XX}.bam
```

All these steps were wrapped in the script `mapdoitall.slurm`, which was provided by the course supervisor. 


## SNP Identification
Identification of SNPs (single nucleotide polymorphisms) was done using `bcftools` as 

```bash 
bcftools mpileup -Ou --threads 8 -f R64-1-1.104.fa ${XX} | bcftools call -vc -Oz --threads 8 -o ${XX}.vcf.gz
# indexing
tabix ${XX}.vcf.gz
```

These steps were encoded in the script `single_snpcall.slurm`. 

## SNP Classification
Annotation of SNPs was done using `SnpSift`. Therewhile, results were filtered to only contain matches in coding regions and hits that were not common to all mutants analysed. 

Classification steps were encoded in the script `single_annotate.slurm`. 

## Data Analysis in Brief 
---
Data analysis code can be found in the `Analaysis` directory. Analysis was done using the sorted data structure within this repository, as opposed to the original structure on the server. 

A cleaned notebook following the main steps described in the report can be found as `cleaned_analysis.ipynb` within the `Analysis` directory. A notebook containing also various other approaches whose results were ultimately discarded can be inspected as `raw_analysis.ipynb` in the `archive` subfolder insider the `Analysis` directory.

