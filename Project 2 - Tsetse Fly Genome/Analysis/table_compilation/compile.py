"""
This script compiles a total csv file from all TOP10 files provided...
"""

import pandas as pd 
import glob, os 
import re

src_folder = "../GO_annotations"

all_top10s = glob.glob(os.path.join(src_folder , "*_TOP10_*.csv"))

# get the GO terms from filenames... (for additional identifier column...)
pattern = "s/GO_(.+)_TOP10"
pattern = re.compile(pattern)
GO_terms = [pattern.search(i).group(1) for i in all_top10s]

total = pd.read_csv(all_top10s[0])
total["GO"] = [GO_terms[0] for i in range(len(total))]
for file, GOterm in zip(all_top10s[1:], GO_terms[1:]):
    tmp = pd.read_csv(file)
    tmp["GO"] = [GOterm for i in range(len(tmp))]
    if len(tmp.index) > 1:
        if len(tmp.index) > 10:
            tmp = tmp.head(10)
            
        total = pd.concat([total, tmp], ignore_index = True)

total.to_csv("compiled.csv", index = False)

# now format into tex-convertable form ...

tex_ready = total.drop(columns=['Gaps', "Mismatches", "% Identical", "Alignment Length"])

columns_remaining = list(tex_ready.columns)
new_total = None
for term in list(set(tex_ready["GO"])):
    tmp = tex_ready.query(f"GO == '{term}'")

    header = pd.DataFrame(
        {i : [] for i in columns_remaining}
    )
    header["Entry Name"] = [term]
    tmp = pd.concat([header, tmp])

    if new_total is None: 
        new_total = tmp
    else:
        new_total = pd.concat([new_total, tmp])

new_total["E-value"] = [f"{i:.2e}" for i in new_total["E-value"]]
new_total = new_total.drop(columns=["GO"])

# regex to clean up the ugly _DROME etc...
# from: (_)([A-Z])([A-Z]+)
# to:  ($2\L$3) (has a space at the beginning)

new_total.to_csv("tex_ready.csv", index=False)